import std.stdio;
import gtk.Main;
import ncrep_win;

void main(string[] args)
{
    Main.init(args);
    auto win = new NCrepWin();
    Main.run();
}
