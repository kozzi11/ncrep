module ncrep_win;
import gtk.MainWindow;

class NCrepWin : MainWindow
{
    import gtk.Label;
    import gtk.Adjustment;
    import gtk.SpinButton;
    import gtk.Grid;
    import gtk.Button;
    import gtk.CheckButton;
    import gtk.Main;
    import gtk.FileFilter;
    import gtk.FileChooserDialog;
    import glib.Util;

    Grid grid;
    Label speedLabel, intensityLabel, pathLabel;
    SpinButton speedEntry, intensityEntry;
    Button pathChooserButton, runButton;
    CheckButton recursiveMode;
    string selectedPath;

    this()
    {
        super("Laser");

        grid = new Grid();
        grid.setColumnSpacing(2);
        grid.setRowSpacing(2);
        grid.setProperty("margin", 2);
        this.createControls();
        this.addControls();
        this.setDefaultSize(300, 200);
        this.add(grid);
        this.setPosition(GtkWindowPosition.CENTER_ALWAYS);
        this.showAll();
    }

    void createControls()
    {
        speedLabel = new Label("Rychlost:");
        intensityLabel = new Label("Intenzita:");
        speedEntry = new SpinButton(new Adjustment(200.0, 0.0, 1000.0, 10, 1.0, 0.0), 1.0, 0);
        intensityEntry = new SpinButton(new Adjustment(300.0, 0.0, 1000.0, 10, 1.0, 0.0), 1.0, 0);
        pathChooserButton = new Button("Soubory", &choosePath);
        pathLabel = new Label("");
        runButton = new Button("Spustit",&runButtonSlot);
        recursiveMode = new CheckButton("Rekurzivně");
    }

    void addControls()
    {
        speedLabel.setHalign(GtkAlign.START);
        intensityLabel.setHalign(GtkAlign.START);

        speedEntry.setHalign(GtkAlign.FILL);
        speedEntry.setValign(GtkAlign.FILL);
        speedEntry.setHexpand(true);
        speedEntry.setVexpand(true);

        intensityEntry.setHalign(GtkAlign.FILL);
        intensityEntry.setValign(GtkAlign.FILL);
        intensityEntry.setHexpand(true);
        intensityEntry.setVexpand(true);

        recursiveMode.setHalign(GtkAlign.FILL);
        recursiveMode.setValign(GtkAlign.FILL);
        recursiveMode.setHexpand(true);
        recursiveMode.setVexpand(true);

        pathChooserButton.setHalign(GtkAlign.FILL);
        pathChooserButton.setValign(GtkAlign.FILL);
        pathChooserButton.setHexpand(true);
        pathChooserButton.setVexpand(true);

        runButton.setHalign(GtkAlign.FILL);
        runButton.setValign(GtkAlign.FILL);
        runButton.setHexpand(true);
        runButton.setVexpand(true);

        grid.attach(speedLabel,         0, 0, 1, 1);
        grid.attach(speedEntry,         1, 0, 1, 1);
        grid.attach(intensityLabel,     0, 1, 1, 1);
        grid.attach(intensityEntry,     1, 1, 1, 1);

        grid.attach(recursiveMode,      0, 2, 2, 1);

        grid.attach(pathLabel,          0, 3, 2, 1);
        grid.attach(pathChooserButton,  0, 4, 2, 1);
        grid.attach(runButton,          0, 5, 2, 1);
    }


    void choosePath(Button btn)
    {
        FileFilter filter = new FileFilter();
        filter.addPattern("*.nc");

        FileChooserDialog dialog = new FileChooserDialog(btn.getLabel, null, FileChooserAction.OPEN);
        const int MY_SELECTED = 0;
        dialog.addButton("Použij složku", MY_SELECTED);
        dialog.setFilter(filter);
        dialog.showAll();
        auto resp = dialog.run;
        if ( resp == ResponseType.OK)
        {
            selectedPath = dialog.getFilename;
            pathLabel.setLabel(selectedPath);
        }
        else if (resp == MY_SELECTED)
        {
            selectedPath = dialog.getFilename;
            if (selectedPath.length == 0) {
                selectedPath = dialog.getCurrentFolder;
            }
            pathLabel.setLabel(selectedPath);
        }
        dialog.destroy();
    }

    void runButtonSlot(Button btn)
    {
        import std.path : buildNormalizedPath;
        import std.file;

        grid.setProperty("sensitive", false);
        if (exists(selectedPath))
        {
            string speed = speedEntry.getText;
            string intensity = intensityEntry.getText;

            if (isDir(selectedPath))
            {
                // Prochazi jen dany adresar
                auto entries = dirEntries(selectedPath,"*.nc",recursiveMode.getActive ? SpanMode.breadth : SpanMode.shallow);
                foreach (entry; entries)
                {
                    processFile(entry.name, speed, intensity);
                }
                doneDialog();
            }
            else
            {
                processFile(selectedPath, speed, intensity);
                doneDialog();
            }
        }
        grid.setProperty("sensitive", true);
    }

    void processFile(string filePath, string speed, string intensity)
    {
        import std.file;
        import std.regex;

        string content = readText(filePath);

        content = replaceAll(content, regex(r"(\nG4 P[0-9]+\.?[0-9]*)", "g"), "");
        content = replaceAll(content, regex(r"(G1 Z[0-9]+\.?[0-9]* F[0-9]+\.?[0-9]*)", "g"), "M4 S1");
        content = replaceAll(content, regex(r"(^G21\nG90\nM4 S1)", "g"), "G21\nG90");
        content = replaceAll(content, regex(r"(G21\nG90\nM4 S1)", "g"), "M4 S1");
        content = replaceAll(content, regex(r"(G1 Z-[0-9]+\.?[0-9]* F[0-9]+\.?[0-9]*)", "g"), "M4 S200");
        content = replaceAll(content, regex(r"(S1)", "g"), "SS");
        content = replaceAll(content, regex(r"(S[0-9]+)", "g"), "S" ~ speed);
        content = replaceAll(content, regex(r"(SS)", "g"), "S1");
        content = replaceAll(content, regex(r"(F[0-9]+\.?[0-9]*)", "g"), "F" ~ intensity ~ ".0");

        write(filePath, content);
    }

    void doneDialog()
    {
        import gtk.MessageDialog;
        auto msgDialog = new MessageDialog(this, GtkDialogFlags.MODAL, GtkMessageType.INFO, GtkButtonsType.OK, "Hotovo");
        msgDialog.run();
        msgDialog.destroy;
    }
}
